---
id: 2019-06-05T23:55:39.974Z
tags: NULL
categories: NULL
---

This is a note.

* [Link with filename + extension](second-task.md)
* [Link with filename](second-task)
* [Link with filename](second-task/)
* [Link to anchor](second-task.md#anchor-sao-goncalo)
* [Link to pdf file](bib/cgu2010.pdf)
* [Link to pdf page](bib/cgu2010.pdf#page=6)
* ![Link to image](img/6049aca4360436077ff04d5e19730247.png)
* ![Link to external image](https://upload.wikimedia.org/wikipedia/commons/8/80/Wikipedia-logo-v2.svg)
