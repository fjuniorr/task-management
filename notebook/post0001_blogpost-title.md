# Sumário

Descrição resumida sobre o tema do blogpost.

# Problema / Motivação

At any given moment, there is always a line representing what your boss will believe. If you step over it, you will not get your budget. Go as close to that line as you can.

Here you have a double responsibility. You must no cross the line and push the line further.
